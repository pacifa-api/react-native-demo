# Pacifa SDK React-native App demo

React-native app using the Pacifa SDK (Data & React-Native Component)

The application start with a top view, after a short animation the roof disappear and all the blocks areas appear.
Once one of theses area is touched the view will change for the selected block view with seats areas displayed.
For both views (SVG type view), externals controls are available.
Once one of theses seat area is touched the view will change to be a panoramic view from the seat point of view. No externals controls are availabke for the panoramic view, they already are in the panoramic view itself.
When a panoramic view is displayed you can add it to a cart component that will list selected seat. A button "show" allow you to open an another component panoramic under the cart to show the view from the selected seat.

Some random design are applied to the areas of both svg view (random selected areas to show custom style)

# Getting Started

Clone the repository and `cd react-native-demo`.

Run `npm install` or `yarn install`.

This project require dependencies from our own repository, to configure the repository follow the documentation : [Where to start](https://sdk.pacifa3d.com/docs/getting-started/where-to-start)

Open the project and In `./config.json` Add the API KEY.

## iOS

In the `ios/` folder run : `pod install`

To run the app : `yarn run ios` or `npm run ios`

## Build Release

To release the app, open the workspace xcode.

Change the scheme to release:

`Product -> Scheme -> Edit Scheme -> Run -> Build release && uncheck Debug executable`

You also need to modify the Info.plist by removing localhost from the `App Transport Security Setting`'s  `Exeption Domain`


## Android

To run the app : `yarn run android` or `npm run android`

## Build Release :

Follow the step from the react-native documentation: [here](https://reactnative.dev/docs/signed-apk-android)